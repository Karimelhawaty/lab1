package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;

public class ISortImp<T extends Comparable<T>> implements ISort<T> {
    @Override
    public IHeap<T> heapSort(ArrayList<T> arrayList) {
        IHeapImp<T> heap = new IHeapImp<>();
        heap.build(arrayList);
        int temp = heap.size();
        for(int i = heap.size(); i >= 2;  i--){
            heap.swap(1, i);
            heap.setSize(i-1);
            heap.heapify(heap.getRoot());
        }
        heap.setSize(temp);
        return heap;
    }

    @Override
    public void sortSlow(ArrayList<T> arrayList) {
        if(arrayList == null){
            return;
        }
        for(int i = 0; i < arrayList.size()-1; i++){
            for(int j = 0; j < arrayList.size()-1; j++){
                if(arrayList.get(j).compareTo(arrayList.get(j+1)) > 0){
                    T temp = arrayList.get(j+1);
                    arrayList.set(j+1, arrayList.get(j));
                    arrayList.set(j, temp);
                }
            }
        }
    }

    @Override
    public void sortFast(ArrayList<T> arrayList) {
        if (arrayList==null)
            return;
        mergeSort(0, arrayList.size()-1, arrayList);
    }
    private void mergeSort(int s, int e, ArrayList<T> arrayList){
        if(s < e){
            int m = (s+e)/2;
            mergeSort(s, m, arrayList);
            mergeSort(m+1, e, arrayList);
            merge(s, e, arrayList);
        }
    }
    private void merge(int s, int e, ArrayList<T> arrayList){
        int m = (s+e)/2;
        ArrayList<T> tmp = new ArrayList<T>();
        int i = s, j = m+1;
        while(i <= m && j <= e){
            if(arrayList.get(i).compareTo(arrayList.get(j)) < 0){
                tmp.add(arrayList.get(i++));
            }else{
                tmp.add(arrayList.get(j++));
            }
        }
        while (i <= m){
            tmp.add(arrayList.get(i++));
        }
        while (j <= e){
            tmp.add(arrayList.get(j++));
        }
        int k = 0;
        for(int it = s; it <= e; ++it){
            arrayList.set(it, tmp.get(k++));
        }
    }
}