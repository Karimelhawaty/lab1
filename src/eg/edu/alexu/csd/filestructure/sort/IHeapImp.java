package eg.edu.alexu.csd.filestructure.sort;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IHeapImp<T extends Comparable<T>> implements IHeap<T> {
    private ArrayList<INode<T>> arr;
    private int heapSize;
    IHeapImp(){
        arr = new ArrayList<>();
        arr.add(null);
        heapSize = 0;
    }
    @Override
    public INode<T> getRoot(){
        if(arr.size() <= 1){
            return null;
        }
        return arr.get(1);
    }

    @Override
    public int size() {
        return heapSize;
    }

    @Override
    public void heapify(INode<T> iNode) {
        if(iNode == null){
            return;
        }
        INode<T> left = iNode.getLeftChild();
        INode<T> right = iNode.getRightChild();
        INode<T> max = iNode;
        if(left != null && max.getValue().compareTo(left.getValue()) < 0){
            max = left;
        }
        if(right != null && max.getValue().compareTo(right.getValue()) < 0){
            max = right;
        }
        if(max != iNode){
            swap(iNode, max);
            heapify(iNode);
        }
    }

    @Override
    public T extract() {
        INode<T> root = getRoot();
        if(root == null){
            return null;
        }
        swap(root, arr.get(arr.size()-1));
        arr.remove(arr.size()-1);
        heapSize--;
        if (arr.size() > 1){
            heapify(getRoot());
        }
        return root.getValue();
    }

    @Override
    public void insert(T t) {
        if(t == null){
            return;
        }
        arr.add(new INodeImp<T>(t, arr.size()));
        INode<T> itr = arr.get(arr.size()-1);
        while(itr.getParent() != null && itr.getParent().getValue().compareTo(itr.getValue()) < 0){
            swap(itr, itr.getParent());
        }
        heapSize++;
    }

    @Override
    public void build(Collection<T> collection) {
        if(collection == null){
            return;
        }
        arr.ensureCapacity(collection.size()+100);
        int itIdx = 1;
        Iterator<T> it = collection.iterator();
        while(it.hasNext()){
            arr.add(new INodeImp<>(it.next(), itIdx));
            itIdx++;
        }
        heapSize = collection.size();
        for(int i = (arr.size()-1)/2; i > 0; i--){
            heapify(arr.get(i));
        }

    }
    public void swap(INode<T> node1, INode<T> node2){
        INodeImp<T> nodeImp1 = (INodeImp<T>) node1;
        INodeImp<T> nodeImp2 = (INodeImp<T>) node2;
        int temp1 = nodeImp1.getIndex();
        int temp2 = nodeImp2.getIndex();
        arr.set(temp2, nodeImp1);
        arr.set(temp1, nodeImp2);
        nodeImp1.setIndex(temp2);
        nodeImp2.setIndex(temp1);
    }
    public void swap(int idx, int idx2){
        swap(arr.get(idx), arr.get(idx2));
    }
    public void setSize(int t){
        heapSize = t;
    }

    public class INodeImp<T extends Comparable<T>> implements INode<T> {
        private int index;
        private T   value;
        INodeImp(T value ,int index){
            this.index = index;
            this.value = value;
        }


        @Override
        public INode<T> getLeftChild() {
            if((index << 1) > heapSize){
                return null;
            }
            return (INode<T>) arr.get((index<<1));
        }

        @Override
        public INode<T> getRightChild() {
            if((index<<1)+1 > heapSize){
                return null;
            }
            return (INode<T>) arr.get((index<<1)+1);
        }

        @Override
        public INode<T> getParent() {
            if(index == 1){
                return null;
            }
            return (INode<T>) arr.get(index>>1);
        }

        @Override
        public T getValue() {
            return value;
        }

        @Override
        public void setValue(T t) {
            value = t;
        }
        public int getIndex(){
            return index;
        }
        public void setIndex(int t){
            index = t;
        }
    }


}
