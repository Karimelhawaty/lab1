package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Random;

public class main {
    // 1 10 100 ---- 1e9
    public static void main(String[] args) {
        int n = 1;
        Random r = new Random();
        ISort<Integer> sortObject = new ISortImp<>();
        while (n <= 10000000) {
            long timeElapsedHeap = 0;
            long timeElapsedFastSort = 0;
            long timeElapsedSlowSort = 0;
            int it = 0;
            while (it < 10) {
                ArrayList<Integer> arr = new ArrayList<>();
                for (int i = 0; i < n; i++) {
                    arr.add(r.nextInt(Integer.MAX_VALUE));
                }

                ArrayList<Integer> arr2 = (ArrayList<Integer>) arr.clone();
                long startTime, stopTime,elapsedTime;
                startTime = System.currentTimeMillis();
                sortObject.heapSort(arr2);
                stopTime = System.currentTimeMillis();
                elapsedTime = stopTime - startTime;
                timeElapsedHeap += elapsedTime;
                arr2 = (ArrayList<Integer>) arr.clone();
                startTime = System.currentTimeMillis();
                sortObject.sortFast(arr2);
                stopTime = System.currentTimeMillis();
                elapsedTime = stopTime - startTime;
                timeElapsedFastSort += elapsedTime;

                it++;
                if(n >= 100000)
                    continue;
                arr2 = (ArrayList<Integer>) arr.clone();
                startTime = System.currentTimeMillis();
                sortObject.sortSlow(arr2);
                stopTime = System.currentTimeMillis();
                elapsedTime = stopTime - startTime;
                timeElapsedSlowSort += elapsedTime;

            }
            timeElapsedHeap /= 10;
            timeElapsedFastSort /= 10;
            timeElapsedSlowSort /= 10;
            System.out.print("Input size = " + n + " : " + "  ");
            System.out.println(timeElapsedHeap + "  " + timeElapsedFastSort + " " + timeElapsedSlowSort);
            n *= 10;
        }
    }


}
